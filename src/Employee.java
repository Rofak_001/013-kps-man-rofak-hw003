import com.bethecoder.ascii_table.ASCIITable;

import java.util.*;
public class Employee {
    private String option;
    private Scanner input;
    private int stfID;
    private String stfName;
    private String stfAddress;
    private int stfHourWork;
    private double stfRate;
    private double stfSalary;
    private double stfBounds;
    private StaffMember stfMember;
    private ArrayList<StaffMember> arrayList;
    private final String [] voluteerHeader ={ "Id",
            "Name", "Address","Other"
    };
    private final String [] salariedEmpHeader={
      "Id","Name","Address","Salary" ,"Bounds","Payment"
    };
    private final String [] hourlyEmpHeader={
            "Id","Name","Address","HourWork","Rate","Payment"
    };
    private ArrayList<Volunteer> volunteersList;
    private ArrayList<SalariedEmployee> salariedEmpList;
    private ArrayList<HourlyEmployee> hourlyEmpList;
    public Employee(){
       StaffMember volunteer1=new Volunteer(1,"Hok LyHang","Phnom Pneh");
       StaffMember hourlyEmployee1=new HourlyEmployee(2,"Kok Dara","Kompong Cham",30,10);
       StaffMember salariedEmployee1=new SalariedEmployee(3,"Anna","Phnom Penh",200,20);
       StaffMember volunteer2=new Volunteer(4,"Bopha","Phnom Pneh");
       StaffMember hourlyEmployee2=new HourlyEmployee(5,"Chantha","Kompot",50,20);
       StaffMember salariedEmployee2=new SalariedEmployee(6,"Thida","Phnom Penh",600,20);
       arrayList=new ArrayList <>();
       arrayList.add(volunteer1);
       arrayList.add(hourlyEmployee1);
       arrayList.add(salariedEmployee1);
       arrayList.add(volunteer2);
       arrayList.add(hourlyEmployee2);
       arrayList.add(salariedEmployee2);
       while (true){
           display();
            menu();
        }
    }

    public void menu(){
        input=new Scanner(System.in).useDelimiter("\n");
        System.out.println("======================MENU====================");
        System.out.println("1. Add Employee");
        System.out.println("2. Update Employee");
        System.out.println("3. Remove Employee");
        System.out.println("4. Exit");
        System.out.println("==============================================");
        System.out.print("=> Choose Option(1-4):");
        option=input.nextLine();
        switch (option) {
            case "1":
                empMenu();
                break;
            case "2":
                update();
                break;
            case "3":
                remove();
                break;
            case "4":
                System.exit(0);
                break;
            default:
                System.out.println("=> Please Input Number (1-4)");
                promptEnterKey();
                break;
        }
    }
    public void empMenu(){
        System.out.println("===========Insert Information================");
        System.out.println("1. Volunteer");
        System.out.println("2. Hourly Emp");
        System.out.println("3. Salaried Emp");
        System.out.println("4. Back");
        System.out.print("=> Choose Option(1-4):");
        option=input.nextLine();
        switch (option){
            case "1":
                addVolunteer();
                break;
            case "2":
                addHourlyEmp();
                break;
            case "3":
                addSalariedEmp();
                break;
            case "4":
                return;
            default:
                System.out.println("=> Please Input Number(1-4)");
                promptEnterKey();
                break;
        }
    }
    public void addVolunteer(){
        System.out.println("=============Insert Info=================");
        //Input id from function is number();
        stfID=isNumber("=> Enter Staff Member's ID:");
        System.out.print("=> Enter Staff Member's Name:");
        stfName=input.next();
        System.out.print("=> Enter Staff Member's Address:");
        stfAddress=input.next();
        boolean check=isExistID(stfID);
        if(check){
            System.out.println("=> This ID:"+stfID +" is Already Exist! Please Use Another ID");
            promptEnterKey();
        }else {
            arrayList.add(new Volunteer(stfID,firstLetterUpper(stfName),stfAddress));
            System.out.println("=> Add Successfully!");
            promptEnterKey();
        }

    }
    public void addHourlyEmp(){
        System.out.println("=============Insert Info=================");
        //Input ID from function isNumber()
        stfID=isNumber("=> Enter Staff Member's ID:");
        System.out.print("=> Enter Staff Member's Name:");
        stfName=input.next();
        System.out.print("=> Enter Staff Member's Address:");
        stfAddress=input.next();
        stfHourWork=isNumber("=> Enter Staff Member's Hour Worked:");
        stfRate=isNumber("=> Enter Rate:");
        boolean check=isExistID(stfID);
        if(check){
            System.out.println("=> This ID:"+stfID +" is Already Exist! Please Use Another ID");
            promptEnterKey();
        }else {
            arrayList.add(new HourlyEmployee(stfID,firstLetterUpper(stfName),stfAddress,stfHourWork,stfRate));
            System.out.println("=> Add Successfully!");
            promptEnterKey();

        }
    }
    public void addSalariedEmp(){
        System.out.println("=============Insert Info=================");
        //Input Id from function isNumber();
        stfID=isNumber("=> Enter Staff Member's ID:");
        System.out.print("=> Enter Staff Member's Name:");
        stfName=input.next();
        System.out.print("=> Enter Staff Member's Address:");
        stfAddress=input.next();
//        System.out.print("=> Enter Staff Salary:");
//        stfSalary=input.nextDouble();
        stfSalary=isNumber("=> Enter Staff Salary:");
//        System.out.print("=> Enter Staff Bounds:");
//        stfBounds=input.nextDouble();
        stfBounds=isNumber("=> Enter Staff Bounds:");
        boolean check=isExistID(stfID);
        if(check){
            System.out.println("=> This ID:"+stfID +" is Already Exist! Please Use Another ID");
            promptEnterKey();
        }else {
            arrayList.add(new SalariedEmployee(stfID,firstLetterUpper(stfName),stfAddress,stfSalary,stfBounds));
            System.out.println("=> Add Successfully!");
            promptEnterKey();
        }
    }
    public String firstLetterUpper(String str){
        String output = str.substring(0, 1).toUpperCase() + str.substring(1);
        return output;
    }
    public void display(){
            Collections.sort(arrayList,new MyNameComp());
//        for(int i=0;i<arrayList.size();i++){
//            System.out.println(arrayList.get(i).toString());
//            System.out.println("=======================================");
//        }
            addTotable();
    }
    public void update(){
        boolean found=false;
        int updateID;
        System.out.println("====================Update Info================");
        updateID=isNumber("=> Enter Id Employee to Update: ");
        for(int i=0;i<arrayList.size();i++){
            int id=arrayList.get(i).getId();
            if(id==updateID){
                if(arrayList.get(i).getClass().getName()=="Volunteer"){
                    System.out.println("==============Volunteer================");
                    System.out.println(arrayList.get(i).toString());
                    System.out.print("= >Enter Staff Member's Name: ");
                    stfName=input.next();
                    System.out.print("= >Enter Staff Member's Address: ");
                    stfAddress=input.next();
                    arrayList.get(i).setName(firstLetterUpper(stfName));
                    arrayList.get(i).setAddress(stfAddress);
                    found=true;
                    System.out.println("=> Updated Successfully!");
                    promptEnterKey();
                }else if(arrayList.get(i).getClass().getName()=="SalariedEmployee"){
                    SalariedEmployee salaryEmp=(SalariedEmployee)arrayList.get(i);
                    System.out.println("==============Salaried Employee Info================");
                    System.out.println(salaryEmp.toString());
                    System.out.println("=================Update Info========================");
                    System.out.print("=> Enter Staff Member's Name: ");
                    stfName=input.next();
                    System.out.print("=> Enter Staff Member's Address: ");
                    stfAddress=input.next();
//                    System.out.print("Enter Staff Salary: ");
//                    stfSalary=input.nextDouble();
                    stfSalary=isNumber("=> Enter Staff Salary: ");
//                    System.out.print("Enter Staff Bounds: ");
//                    stfBounds=input.nextDouble();
                    stfBounds=isNumber("=> Enter Staff Bounds: ");
                    arrayList.set(i,new SalariedEmployee(salaryEmp.getId(),firstLetterUpper(stfName),stfAddress,stfSalary,stfBounds));
                    found=true;
                    System.out.println("=> Updated Successfully!");
                    promptEnterKey();
                }else if(arrayList.get(i).getClass().getName()=="HourlyEmployee"){
                    HourlyEmployee hourEmp=(HourlyEmployee)arrayList.get(i);
                    System.out.println("==============Hourly Employee Info================");
                    System.out.println(hourEmp.toString());
                    System.out.println("=================Update Info=======================");
                    System.out.print("=> Enter Staff Member's Name: ");
                    stfName=input.next();
                    System.out.print("=> Enter Staff Member's Address: ");
                    stfAddress=input.next();
//                    System.out.print(" Enter Staff Member's Hour Worked:");
//                    stfHourWork=input.nextInt();
                    stfHourWork=isNumber("=> Enter Staff Member's Hour Worked:");
//                    System.out.print("Enter Rate:");
//                    stfRate=input.nextDouble();
                    stfRate=isNumber("=> Enter Rate:");
                    arrayList.set(i,new HourlyEmployee(hourEmp.getId(),firstLetterUpper(stfName),stfAddress,stfHourWork,stfRate));
                    found=true;
                    System.out.println("=> Updated Successfully!");
                    promptEnterKey();
                }
            }
        }
        if(found==false){
            System.out.println("=> Updated Failed! No Employee in this ID: "+updateID);
            promptEnterKey();
        }
    }
    public void remove(){
        boolean found =false;
        System.out.println("=================DELETE======================");
        stfID=isNumber("=> Enter Employee ID to Remove: ");
        for(int i=0;i<arrayList.size();i++){
            if(stfID==arrayList.get(i).getId()){
                System.out.println(arrayList.get(i).toString());
                arrayList.remove(i);
                System.out.println("=> Removed Successfully!");
                found=true;
                promptEnterKey();
            }
        }
        if(found==false){
            System.out.println("=> Removed Failed! No Employee in this ID: "+stfID);
            promptEnterKey();
        }
    }
    public void promptEnterKey(){
        System.out.print("=> Press \"ENTER\" to continue...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
    public boolean isExistID(int checkId){
        boolean check=false;
        for(int i=0;i<arrayList.size();i++){
            int id=arrayList.get(i).getId();
            if(checkId==id) {
                check = true;
            }
        }
        return check;
    }
    public static boolean isNumeric(String str) {
        return str != null && str.matches("[-+]?\\d*\\.?\\d+");
    }
    public int isNumber(String str){
        boolean isNotNumber=true;
        int value=0;
        while (isNotNumber){
            System.out.print(str);
            String myValue=input.next();
            if (isNumeric(myValue)){
                value=Integer.parseInt(myValue);
                isNotNumber=false;
            }else {
                System.out.println("=> Please Input Number!");
                isNotNumber=true;
            }
        }
        return value;
    }
    public void addTotable(){
        volunteersList=new ArrayList <>();
        salariedEmpList=new ArrayList <>();
        hourlyEmpList=new ArrayList <>();
        for (int i=0;i<arrayList.size();i++){
            if(arrayList.get(i).getClass().getName()=="Volunteer"){
                Volunteer volunteer=(Volunteer)arrayList.get(i);
                volunteersList.add(volunteer);
            }else if(arrayList.get(i).getClass().getName()=="SalariedEmployee"){
                SalariedEmployee salariedEmp=(SalariedEmployee)arrayList.get(i);
                salariedEmpList.add(salariedEmp);
            }else if(arrayList.get(i).getClass().getName()=="HourlyEmployee"){
                HourlyEmployee hourlyEmp=(HourlyEmployee)arrayList.get(i);
                hourlyEmpList.add(hourlyEmp);
            }
        }
        //Table Volunteer//
        Collections.sort(volunteersList,new MyNameComp());
        if(volunteersList.size()!=0){
            String[][] data=new String[volunteersList.size()][voluteerHeader.length];
            for(int i=0;i<data.length;i++){
                int id=volunteersList.get(i).getId();
                data[i][0]=String.valueOf(id);
                data[i][1]=volunteersList.get(i).getName();
                data[i][2]=volunteersList.get(i).getAddress();
                data[i][3]="Thank !";
            }
            System.out.println("======================Volunteer Emp=======================");
            System.out.println();
            ASCIITable.getInstance().printTable(voluteerHeader, data);
        }else {
            System.out.println("=> No Volunteer Employee!");
            System.out.println();
        }
        //Table Salaried Emp
        salaryEmpTable(salariedEmpList);
        //Table Hourly Emp
        hourlyEmpTable(hourlyEmpList);
    }
    public void salaryEmpTable(ArrayList<SalariedEmployee> emplist ){
        Collections.sort(emplist,new MyNameComp());
        if(emplist.size()!=0){
            String[][] data=new String[emplist.size()][salariedEmpHeader.length];
            for(int i=0;i<data.length;i++){
                int id=emplist.get(i).getId();
                data[i][0]=String.valueOf(id);
                data[i][1]=emplist.get(i).getName();
                data[i][2]=emplist.get(i).getAddress();
                data[i][3]=String.valueOf(emplist.get(i).getSalary());
                data[i][4]=String.valueOf(emplist.get(i).getBounds());
                data[i][5]=String.valueOf(emplist.get(i).pay());
            }
            System.out.println("========================Salaried Emp======================");
            System.out.println();
            ASCIITable.getInstance().printTable(salariedEmpHeader, data);
        }else {
            System.out.println("=> No Salaried Employee!");
            System.out.println();
        }
    }
    public void hourlyEmpTable(ArrayList<HourlyEmployee> emplist ){
        Collections.sort(emplist,new MyNameComp());
        if(emplist.size()!=0){
            String[][] data=new String[emplist.size()][hourlyEmpHeader.length];
            for(int i=0;i<data.length;i++){
                int id=emplist.get(i).getId();
                data[i][0]=String.valueOf(id);
                data[i][1]=emplist.get(i).getName();
                data[i][2]=emplist.get(i).getAddress();
                data[i][3]=String.valueOf(emplist.get(i).getHourWorked());
                data[i][4]=String.valueOf(emplist.get(i).getRate());
                data[i][5]=String.valueOf(emplist.get(i).pay());
            }
            System.out.println("=========================Hourly Emp=========================");
            System.out.println();
            ASCIITable.getInstance().printTable(hourlyEmpHeader, data);
        }else {
            System.out.println("=> No Hourly Employee!");
            System.out.println();
        }
    }
}

