public class Volunteer extends StaffMember {

    public Volunteer(int id, String name, String address) {
        super(id, name, address);

    }

    @Override
    public double pay() {
        return 0;
    }

    @Override
    public String toString() {
        return
                "id: " + id +"\n"+
                "name: " + name + '\n' +
                "address: " + address + '\n'+
                "Thank !"
                ;
    }
}
