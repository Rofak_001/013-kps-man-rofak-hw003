public class HourlyEmployee extends StaffMember {
    private int hourWorked;
    private double rate;
    public HourlyEmployee(int id, String name, String address,int hourWorked,double rate) {
        super(id, name, address);
        this.hourWorked=hourWorked;
        this.rate=rate;
    }

    public int getHourWorked() {
        return hourWorked;
    }

    public void setHourWorked(int hourWorked) {
        this.hourWorked = hourWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public double pay() {
        return this.getHourWorked()*this.rate;
    }

    @Override
    public String toString() {
        return
                "id: " + id +"\n"+
                "name:" + name + '\n' +
                "address: " + address + '\n' +
                "hourWorked: " + hourWorked +"\n"+
                "rate: " + rate+"\n"+
                "Payment: "+pay()
                ;
    }
}
