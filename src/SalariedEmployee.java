public class SalariedEmployee extends StaffMember {
    private double salary;
    private double bounds;

    public SalariedEmployee(int id, String name, String address,double salary,double bounds) {
        super(id, name, address);
        this.salary=salary;
        this.bounds=bounds;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBounds() {
        return bounds;
    }

    public void setBounds(double bounds) {
        this.bounds = bounds;
    }

    @Override
    public double pay() {
        return this.getSalary()+this.bounds;
    }

    @Override
    public String toString() {
        return
                "id: " + id +"\n"+
                "name:" + name + '\n' +
                "address: " + address + '\n' +
                "salary: " + salary +"\n"+
                "bounds: " + bounds+"\n"+
                 "Payment: "+pay()
                ;
    }
}
